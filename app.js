const express =  require("express");
const app = express();

app.use(express.urlencoded({ extended: false }));
app.use(express.json());

const mongoose = require("mongoose");
require("dotenv").config();

const port = process.env.PORT || 8079;

// routes
app.use("/api", require("./src/routes"));




mongoose.connect(process.env.DB_DATABASE, {
    useNewUrlParser: true,
    // useCreateIndex: true
}).then(() => {
    console.log("DB connected!")
}).catch((err) => {
    console.log(err)
});

// app.get("/access-token", (req, res) => {
//   // access token
//   let url = "https://sandbox.safaricom.co.ke/oauth/v1/generate?grant_type=client_credentials";

//   axios({
//       url: url,
//       headers: {
//         Authorization: "Basic " + "MWc3MWdCT1d1a3FvM1Bpa0ZUVXZVM01xUVZGbGZNWUE6WWxKMmpPanoyc2VBSTNjTQ=="
//       }
//   })
//   .then((data )=> {
//     res.send(data.data)
//   })
//   .catch(error => console.log(error));
// })

app.listen(port, () => {
  console.log(`App is running on port ${port}`);
})

module.exports = app;