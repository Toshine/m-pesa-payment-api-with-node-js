require("dotenv").config();
const datetime = require("node-datetime");
const axios = require("axios");

const passkey = process.env.PASSKEY;
const consumerKey = process.env.CONSUMER_KEY;
const consumerSecret = process.env.CONSUMER_SECRET;
const shortcode = process.env.SHORTCODE;

let formatted;


const newPassword = () => {
    const dt = datetime.create();
    const formatted = dt.format('YmdHMS');

    const passString = shortcode + passkey + formatted;
    const base64EncodedPassword = Buffer.from(passString).toString("base64");
    const asciiFormat = Buffer.from(
        "MTc0Mzc5TVdjM01XZENUMWQxYTNGdk0xQnBhMFpVVlhaVk0wMXhVVlpHYkdaTldVRTZXV3hLTW1wUGFub3ljMlZCU1ROalRRPT0yMDIxMTEwMjE3MTMyNA==",
        "base64"
        ).toString("ascii")

    return base64EncodedPassword;
}

exports.token = (req, res, next) => {

    let url = 'https://sandbox.safaricom.co.ke/oauth/v1/generate?grant_type=client_credentials';
    // let auth = "Basic" + Buffer.from(consumerKey + ":" + consumerSecret).toString("base64");
    let auth = "Basic " + "MWc3MWdCT1d1a3FvM1Bpa0ZUVXZVM01xUVZGbGZNWUE6WWxKMmpPanoyc2VBSTNjTQ==";
    let headers = { Authorization: auth }

    
    axios.get(url,{
        headers: headers
    }).then((res) => {
        let data = res.data;
        let accessToken = data.access_token
        req.token = accessToken
        console.log(accessToken)
        next();
    }).catch(error => console.log(error));

}

exports.password = (req, res) => {
    res.send({
        status: "success",
        message: "Request sent successfully",
        password: newPassword()
    });
}

exports.stkPush = (req, res) => {
    const token = req.token;

    const headers = { 
        Authorization: "Bearer " + token
    }

    const stkUrl = "https://sandbox.safaricom.co.ke/mpesa/stkpush/v1/processrequest";

    let data = {
    BusinessShortCode: "174379",
    Password: newPassword(),
    Timestamp: formatted,
    TransactionType: "CustomerPayBillOnline",
    Amount: "120",
    // The sender
    PartyA: "254708374149",
    // The receiver
    PartyB: "174379",
    PhoneNumber: "254708374149",
    CallBackURL: "http://5c1d-129-205-113-4.ngrok.io/",
    AccountReference: "Test",
    TransactionDesc: "Test"
    }

    axios.post(stkUrl, data, { headers: headers })
    .then((response) => {
        res.send(response.data)
    }).catch(err => console.log(err));
    // res.send(token)
}



// Register C2B URL
exports.c2BRegisterUrl = (req, res) => {
    const token = req.token;


    const headers = { 
        Authorization: "Bearer " + token
    }

    const url = "https://sandbox.safaricom.co.ke/mpesa/c2b/v1/registerurl";

    let data = {
        ShortCode: "600992",
        ResponseType: "Completed",
        ConfirmationURL: "http://d6d4-197-210-54-7.ngrok.io",
        ValidationURL: "http://d6d4-197-210-54-7.ngrok.io/v"
    }

    axios.post(url, data, { headers: headers })
    .then((response) => {
        res.send(response.data)
    }).catch(err => console.log(err));
}


// Make C2B Payment Request
exports.c2BPaymentRequest = (req, res) => {
    const token = req.token;


    const headers = { 
        Authorization: "Bearer " + token
    }

    const url = "https://sandbox.safaricom.co.ke/mpesa/c2b/v1/simulate";

    let data = {
    ShortCode:"600992",
    CommandID:"CustomerPayBillOnline",
    Amount:"300",
    Msisdn:"254708374149",
    BillRefNumber:"h6dk0Ue2"

    }

    axios.post(url, data, { headers: headers })
    .then((response) => {
        res.send(response.data)
    }).catch(err => console.log(err));
}


// module.exports = password;