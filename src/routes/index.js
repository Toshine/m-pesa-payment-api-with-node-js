const express = require("express");
const router = express.Router();
// const axios = require("axios");

const {
    password,
    token,
    stkPush,
    c2BRegisterUrl,
    c2BPaymentRequest
} = require("../controllers/mpesa");

router.get("/", (req, res) => {
    res.send("Hello Mpesa");
})

// router.get("/access-token", (req, res) => {
//   // access token
//   let url = "https://sandbox.safaricom.co.ke/oauth/v1/generate?grant_type=client_credentials";

//   axios({
//       url: url,
//       headers: {
//         Authorization: "Basic " + "MWc3MWdCT1d1a3FvM1Bpa0ZUVXZVM01xUVZGbGZNWUE6WWxKMmpPanoyc2VBSTNjTQ=="
//       }
//   })
//   .then((data )=> {
//     res.send(data.data)
//   })
//   .catch(error => console.log(error));
// })

router.get("/password", password);

router.post("/stk/push", token, stkPush)

router.post("/c2b/register/url", token, c2BRegisterUrl)

router.post("/c2b/payment/request", token, c2BPaymentRequest)

module.exports = router;