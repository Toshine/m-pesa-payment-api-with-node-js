const mongoose = require("mongoose");
require("dontenv").config();


mongoose.connect(process.env.DB_DATABASE, {
    useNewUrlParser: true,
    useCreateIndex: true
}).then(() => {
    console.log("DB connected!")
}).catch((err) => {
    console.log(err)
});